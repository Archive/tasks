/*
 * Copyright (C) 2007 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <config.h>
#include <gtk/gtk.h>

#include "window-util.h"

#define GROUP_PROP "taku-window-group"

#define WINDOW_X "WindowX"
#define WINDOW_Y "WindowY"
#define WINDOW_WIDTH "WindowWidth"
#define WINDOW_HEIGHT "WindowHeight"

static const char *
get_filename (void)
{
  static const char *filename = NULL;
  if (filename == NULL) {
    filename = g_build_filename (g_get_user_config_dir (),
                                 g_get_prgname (),
                                 "windows",
                                 NULL);
  }
  return filename;
}

static gboolean
get (GKeyFile *keys, const char *group, const char *key, int *value)
{
  GError *error = NULL;
  int i;

  i = g_key_file_get_integer (keys, group, key, &error);

  if (error) {
    g_error_free (error);
    return FALSE;
  } else {
    *value = i;
    return TRUE;
  }
}

static GKeyFile *
get_key_file (void)
{
  GError *error = NULL;
  const char *filename;
  GKeyFile *keys;

  filename = get_filename ();

  keys = g_key_file_new ();

  if (g_file_test (filename, G_FILE_TEST_EXISTS)) {
    if (!g_key_file_load_from_file (keys, filename, G_KEY_FILE_NONE, &error)) {
      g_warning ("Cannot read key file: %s", error->message);
      g_error_free (error);
    }
  }

  return keys;
}

static void
on_unmap (GtkWidget *widget, gpointer user_data)
{
  GError *error = NULL;
  GKeyFile *keys;
  const char *filename, *group;
  char *dirname, *data;
  int x, y, w, h;

  keys = get_key_file ();

  group = g_object_get_data (G_OBJECT (widget), GROUP_PROP);

  gtk_window_get_position (GTK_WINDOW (widget), &x, &y);
  g_key_file_set_integer (keys, group, WINDOW_X, x);
  g_key_file_set_integer (keys, group, WINDOW_Y, y);

  gtk_window_get_size (GTK_WINDOW (widget), &w, &h);
  g_key_file_set_integer (keys, group, WINDOW_WIDTH, w);
  g_key_file_set_integer (keys, group, WINDOW_HEIGHT, h);

  filename = get_filename ();
  dirname = g_path_get_dirname (filename);
  g_mkdir_with_parents (dirname, 0755);
  g_free (dirname);

  data = g_key_file_to_data (keys, NULL, NULL);
  if (!g_file_set_contents (filename, data, -1, &error)) {
    g_warning ("Cannot write key file: %s", error->message);
    g_error_free (error);
  }

  g_free (data);
  g_key_file_free (keys);
}

void
window_bind_state (GtkWindow *window, const char *name)
{
  GKeyFile *keys;
  char *group;
  gboolean got_pos, got_size;
  int x, y, w, h, screen_w, screen_h;

  g_return_if_fail (GTK_IS_WINDOW (window));

  g_signal_connect (window, "unmap", G_CALLBACK (on_unmap), NULL);

  keys = get_key_file ();

  group = g_strconcat ("Window-", name, NULL);
  g_object_set_data_full (G_OBJECT (window), GROUP_PROP, group, g_free);

  got_pos = get (keys, group, WINDOW_X, &x);
  if (got_pos)
    got_pos &= get (keys, group, WINDOW_Y, &y);

  if (got_pos) {
    gtk_window_move (window, x, y);
  }

  got_size = get (keys, group, WINDOW_WIDTH, &w);
  if (got_size)
    got_size &= get (keys, group, WINDOW_HEIGHT, &h);

  if (got_size) {
    screen_w = gdk_screen_get_width (gtk_window_get_screen (window));
    screen_h = gdk_screen_get_width (gtk_window_get_screen (window));

    if (w > screen_w)
        w = screen_w;

    if (h > screen_h)
        h = screen_h;

    gtk_window_set_default_size (window, w, h);
  }

  g_key_file_free (keys);
}

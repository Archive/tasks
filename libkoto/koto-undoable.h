/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __KOTO_UNDOABLE_H__
#define __KOTO_UNDOABLE_H__

#include <glib.h>
#include <glib-object.h>

typedef void (*KotoUndoableFunc) (gpointer closure);

typedef struct _KotoUndoable {
	/* The functions that will undo/redo the action if they are passed
	 * the data packet that is stored in closure */
	KotoUndoableFunc undo;
	KotoUndoableFunc redo;
	KotoUndoableFunc destroy;

	gpointer closure; /* This is enough data for the undo/redo functions
			     to perform their actions */
} KotoUndoable;

KotoUndoable *koto_undoable_new (KotoUndoableFunc undo,
				     KotoUndoableFunc redo,
				     KotoUndoableFunc destroy,
				     gpointer closure);
void koto_undoable_free (KotoUndoable *undoable);
void koto_undoable_undo (KotoUndoable *undoable);
void koto_undoable_redo (KotoUndoable *undoable);

#endif

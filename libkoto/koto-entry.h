/*
 * Copyright (C) 2007 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KOTO_ENTRY
#define _KOTO_ENTRY

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define KOTO_TYPE_ENTRY koto_entry_get_type()

#define KOTO_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  KOTO_TYPE_ENTRY, KotoEntry))

#define KOTO_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  KOTO_TYPE_ENTRY, KotoEntryClass))

#define KOTO_IS_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  KOTO_TYPE_ENTRY))

#define KOTO_IS_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  KOTO_TYPE_ENTRY))

#define KOTO_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  KOTO_TYPE_ENTRY, KotoEntryClass))

typedef struct {
  GtkEntry parent;
} KotoEntry;

typedef struct {
  GtkEntryClass parent_class;
} KotoEntryClass;

GType koto_entry_get_type (void);

GtkWidget* koto_entry_new (void);

void koto_entry_set_text (KotoEntry *entry, const char *text);

G_END_DECLS

#endif /* _KOTO_ENTRY */

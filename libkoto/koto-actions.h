/*
 * Copyright (C) 2007 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KOTO_ACTIONS
#define _KOTO_ACTIONS

#include <libecal/e-cal.h>
#include "koto-task.h"
#include "koto-undo-manager.h"

G_BEGIN_DECLS

gboolean koto_action_create_task (ECal *cal, icalcomponent *comp, char **uid, KotoUndoContext *undo);

gboolean koto_action_modify_task (ECal *cal, KotoTask *task, icalcomponent *old, KotoUndoContext *undo);

gboolean koto_action_delete_task (ECal *cal, KotoTask *task, KotoUndoContext *undo);

G_END_DECLS

#endif /* _KOTO_ACTIONS */

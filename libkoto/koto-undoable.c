/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "koto-undoable.h"

KotoUndoable *
koto_undoable_new (KotoUndoableFunc undo,
		     KotoUndoableFunc redo,
		     KotoUndoableFunc destroy,
		     gpointer closure)
{
	KotoUndoable *u;

	u = g_slice_new0 (KotoUndoable);
	u->undo = undo;
	u->redo = redo;
	u->destroy = destroy;
	u->closure = closure;

	return u;
}

void
koto_undoable_free (KotoUndoable *undoable)
{
	if (undoable->destroy) {
		undoable->destroy (undoable->closure);
	}

	g_slice_free (KotoUndoable, undoable);
}

void
koto_undoable_undo (KotoUndoable *undoable)
{
	if (undoable->undo) {
		undoable->undo (undoable->closure);
	}
}

void
koto_undoable_redo (KotoUndoable *undoable)
{
	if (undoable->redo) {
		undoable->redo (undoable->closure);
	}
}

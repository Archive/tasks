#include <locale.h>
#include <string.h>
#include "ical-util.h"

static const char *weekdays[] = {
  "BAD WEEKDAY", "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday", "Sunday"
};

int
main (int argc, char **argv)
{
  GDate date;
  char *s;

  /* Set the locale to C so we can do string comparisons easily */
  setlocale(LC_ALL, "C");

  g_date_clear (&date, 1);

  g_date_set_dmy (&date, 1, G_DATE_MAY, 2001);
  s = ical_util_get_human_due_date (&date);
  g_assert (strcmp (s, "due 05/01/01") == 0);

  g_date_set_time_t (&date, time (NULL));
  s = ical_util_get_human_due_date (&date);
  g_assert (strcmp (s, "due today") == 0);

  g_date_add_days (&date, 1);
  s = ical_util_get_human_due_date (&date);
  g_assert (strcmp (s, "due tomorrow") == 0);

  g_date_subtract_days (&date, 2);
  s = ical_util_get_human_due_date (&date);
  g_assert (strcmp (s, "due yesterday") == 0);

  g_date_add_days (&date, 3);
  s = ical_util_get_human_due_date (&date);
  g_assert (strcmp (s, g_strdup_printf ("due %s", weekdays[g_date_get_weekday (&date)])) == 0);

  return 0;
}

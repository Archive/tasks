/*
 * Copyright (C) 2008 OpenedHand Ltd
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Initially based on http://simon.incutio.com/code/js/date-parser/dateparse.js,
 * ported to C by Matthew Allum <mallum@openedhand.com>.
 */

#include <config.h>
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <glib.h>
#include <glib/gi18n.h>
#include "koto-date-parser.h"

/*
 * This entire file is replaced with a stub if we don't have GRegex
 */
/* Set this to 1 to get lots of debugging output */
#define DEBUG 0

typedef GDate* (*MatchFunc) (const char *input, GMatchInfo *info);

typedef struct Matcher {
  GRegex *regex;
  MatchFunc func;
} Matcher;

static GHashTable *day_hash = NULL, *month_hash = NULL;

static void
populate_hash (GHashTable *hash, const char **aliases, int count)
{
  int i;

  g_assert (hash);
  g_assert (aliases);
  g_assert (count);

  for (i = 0; i < count; i++) {
    char **tokens, **s;

    if (aliases[i] == NULL)
      continue;

    tokens = g_strsplit (_(aliases[i]), "|", 0);
    for (s = tokens; *s; s++) {
      g_hash_table_insert (hash, g_utf8_casefold (*s, -1), GINT_TO_POINTER (i));
    }
    g_strfreev (tokens);
  }
}

static void
init_days (void)
{
  const char *aliases[] = {
    /* This means that the indexes match GDateWeekday */
    NULL,
    /* Translators: these strings should have every common form for weekdays
       including abbreviations, separated by | */
    N_("monday|mon"),
    N_("tuesday|tue"),
    N_("wednesday|wed"),
    N_("thursday|thu"),
    N_("friday|fri"),
    N_("saturday|sat"),
    N_("sunday|sun"),
  };

  day_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  populate_hash (day_hash, aliases, G_N_ELEMENTS (aliases));
}

static void
init_months (void)
{
  const char *aliases[] = {
    /* This means that the indexes match GDateMonth */
    NULL,
    /* Translators: these strings should have every common form for month names
       including abbreviations, separated by | */
    N_("january|jan"),
    N_("february|feb"),
    N_("march|mar"),
    N_("april|apr"),
    N_("may"),
    N_("june|jun"),
    N_("july|jul"),
    N_("august|aug"),
    N_("september|sept|sep"),
    N_("october|oct"),
    N_("november|nov"),
    N_("december|dec"),
  };

  month_hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  populate_hash (month_hash, aliases, G_N_ELEMENTS (aliases));
}

static GDateWeekday
convert_str_weekday (const char *s)
{
  char *folded;
  GDateWeekday day;
  g_assert (s);

  folded = g_utf8_casefold (s, -1);
#if DEBUG
  g_printerr ("Converting '%s' to a week day\n", folded);
#endif
  day = GPOINTER_TO_INT (g_hash_table_lookup (day_hash, folded));
#if DEBUG
  g_printerr ("Got %d\n", day);
#endif
  g_free (folded);
  return day;
}

static GDateMonth
convert_str_month (const char *s)
{
  char *folded;
  GDateMonth month;
  g_assert (s);

  folded = g_utf8_casefold (s, -1);
#if DEBUG
  g_printerr ("Converting '%s' to a month\n", folded);
#endif
  month = GPOINTER_TO_INT (g_hash_table_lookup (month_hash, folded));
#if DEBUG
  g_printerr ("Got %d\n", month);
#endif
  g_free (folded);
  return month;
}

static GDate *
today (void)
{
  GDate *date;
  date = g_date_new ();
  /* Enforce a fixed today date when testing for ease */
#if BUILD_TESTS
  g_date_set_dmy (date, 11, 3, 2008);
#else
  g_date_set_time_t (date, time (NULL));
#endif
  return date;
}

static GDate *
match_today (const char *input, GMatchInfo *info)
{
  GDate *date;
  date = today ();
  return date;
}

static GDate *
match_tomorrow (const char *input, GMatchInfo *info)
{
  GDate *date;
  date = today ();
  g_date_add_days (date, 1);
  return date;
}

static GDate *
match_yesterday (const char *input, GMatchInfo *info)
{
  GDate *date;
  date = today ();
  g_date_subtract_days (date, 1);
  return date;
}

static GDate *
match_next (const char  *input, GMatchInfo *info)
{
  GDate *date;
  char *s;
  GDateDay current_day, next_day;
  int add_days;

  s = g_match_info_fetch_named (info, "day");
  next_day = convert_str_weekday (s);
  g_free (s);
  if (next_day == G_DATE_BAD_WEEKDAY)
    return NULL;

  date = today ();
  current_day = g_date_get_weekday (date);
  add_days = 7 + next_day - current_day;

  g_date_add_days (date, add_days);

  return date;
}

static GDate *
match_this (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *s;
  GDateDay today_day;
  GDateWeekday target_day;

  s = g_match_info_fetch_named (info, "day");
  target_day = convert_str_weekday (s);
  g_free (s);
  if (target_day == G_DATE_BAD_WEEKDAY)
    return NULL;

  date = today ();
  today_day = g_date_get_day (date);
  g_date_subtract_days (date, g_date_get_weekday (date));
  g_date_add_days (date, target_day);

  if (g_date_get_day (date) <= today_day)
    g_date_add_days (date, 7);

  return date;
}

static GDate *
match_date (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *day_s, *month_s;
  GDateDay day;
  GDateMonth month;

  day_s = g_match_info_fetch_named (info, "day");
  if (!day_s)
    return NULL;
  day = atoi (day_s);
  g_free (day_s);

  month_s = g_match_info_fetch_named (info, "month");
  if (!month_s)
    return NULL;
  month = atoi (month_s);
  g_free (month_s);

  if (!g_date_valid_day (day) || !g_date_valid_month (month))
    return NULL;

  date = today ();
  g_date_set_day (date, day);
  g_date_set_month (date, month);

  return date;
}

static GDate *
match_day (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *day_s;
  GDateDay day;

  /*
   * TODO: this should handle days in the following month instead of this month
   */

  day_s = g_match_info_fetch_named (info, "day");
  if (!day_s)
    return NULL;
  day = atoi (day_s);
  g_free (day_s);

  if (!g_date_valid_day (day))
    return NULL;

  date = today ();
  g_date_set_day (date, day);

  return date;
}

static GDate *
match_day_month (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *day_s, *month_s;
  GDateDay day;
  GDateMonth month;

  day_s = g_match_info_fetch_named (info, "day");
  if (!day_s)
    return NULL;
  day = atoi (day_s);
  g_free (day_s);

  month_s = g_match_info_fetch_named (info, "month");
  month = convert_str_month (month_s);
  g_free (month_s);

  if (!g_date_valid_day (day) || !g_date_valid_month (month))
    return NULL;

  date = today ();
  g_date_set_day (date, day);
  g_date_set_month (date, month);

  return date;
}

static GDate *
match_digits (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *s;

  s = g_match_info_fetch_named (info, "digits");
  if (!s)
    return NULL;

  date = g_date_new ();
  g_date_set_parse (date, s);
  g_free (s);

  if (g_date_valid (date)) {
    return date;
  } else {
    g_date_free (date);
    return NULL;
  }
}

static GDate *
match_week (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *week_s;
  int week, this_week;

  week_s = g_match_info_fetch_named (info, "week");
  if (!week_s)
    return NULL;
  week = atoi (week_s);
  g_free (week_s);

  if (week < 1 || week > 53)
    return NULL;

  date = today ();
  this_week = g_date_get_iso8601_week_of_year (date);

  /* Go to the Monday in week 1 */
  g_date_set_day (date, 1);
  g_date_set_month (date, G_DATE_JANUARY);
  if (week <= this_week)
    g_date_add_years (date, 1);
  g_date_subtract_days (date, g_date_get_weekday (date) - G_DATE_MONDAY);

  g_date_add_days (date, (week - 1) * 7);

  return date;
}

static GDate *
match_days (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *days_s;
  int days;

  days_s = g_match_info_fetch_named (info, "days");
  if (!days_s)
    return NULL;
  days = atoi (days_s);
  g_free (days_s);

  if (days < 1)
    return NULL;

  date = today ();
  g_date_add_days (date, days);
  return date;
}

static GDate *
match_weeks (const char *input, GMatchInfo *info)
{
  GDate *date;
  char *weeks_s;
  int weeks;

  weeks_s = g_match_info_fetch_named (info, "weeks");
  if (!weeks_s)
    return NULL;
  weeks = atoi (weeks_s);
  g_free (weeks_s);

  if (weeks < 1)
    return NULL;

  date = today ();
  g_date_add_days (date, weeks * 7);
  return date;
}

static Matcher*
matcher_new (const gchar *regex_str, MatchFunc func)
{
  GError *error = NULL;
  GRegex *regex;
  Matcher *m;

  g_assert (regex_str);
  g_assert (func);

  regex = g_regex_new (regex_str,
		       G_REGEX_CASELESS | G_REGEX_OPTIMIZE, 0,
		       &error);

  if (error) {
    g_warning ("Cannot parse regex '%s': %s", regex_str, error->message);
    g_error_free (error);
    return NULL;
  }

  m = g_slice_new (Matcher);
  m->regex = regex;
  m->func = func;
  return m;
}

static GList *
init_matchers (void)
{
  GList *matchers = NULL;
  Matcher *m;

  init_days ();
  init_months ();

  /*
    Translators: WARNING: scary strings ahead.  If you don't understand regular
    expressions then skip these, otherwise
    http://www.burtonini.com/blog/computers/tasks-2008-10-01-21-17 has some
    details about how this works.

    These strings are used to parse the user's input in the "New Task" entry to
    extract the meaning of what they entered, such as the due date.

    The important thing to preserve in the translation are the (?<name>expr)
    blocks, which identify a regular expression "expr" with the identifier
    "name".  These mark sections which will be used in the new task.  All of the
    strings have a block called "task", which is the actual task summary.  Other
    examples are "day" (day of the month), "month" (month name) and so on.
  */

  /* Translators: "Pay bills today".  Just translate "today" */
  m = matcher_new (_("^(?<task>.+) today$"), match_today);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills tomorrow". Just translate "tomorrow" */
  m = matcher_new (_("^(?<task>.+) tomorrow$"), match_tomorrow);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills yesterday".  Just translate "yesterday" */
  m = matcher_new (_("^(?<task>.+) yesterday$"), match_yesterday);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills next Monday". */
  m = matcher_new (_("^(?<task>.+) next (?<day>\\w+)$"), match_next);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills this tuesday".  just translate this|on|by|due to contain relevant words in
     your language, moving the day block is required. */
  m = matcher_new (_("^(?<task>.+) (this|on|by|due) (?<day>\\w+)$"),  match_this);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills by 4/12" (day/month).  Re-arrange as required. */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on) (?<day>\\d{1,2})/(?<month>\\d{1,2})$"),  match_date);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills on 3rd" or "Pay bills on 12".  Not all languages
     have numeric suffixes so feel free to remove them. */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on)? (?<day>\\d{1,2})(?:st|nd|rd|th)?$"),  match_day);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills on 12th January" or "Pay bills on 23 June". */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on)? (?<day>\\d{1,2})(?:st|nd|rd|th)? (?<month>\\w+)$"),  match_day_month);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills on January 12th" or "Pay bills on 23 June".  This
     is for languages where there is two way of writing a day, if there is only
     one correct way then translate this identically to the previous one. */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on)? (?<month>\\w+) (?<day>\\d{1,2})(?:st|nd|rd|th)?$"),  match_day_month);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills on 15/4". */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on)? (?<digits>[\\d/-]+)$"), match_digits);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills by w43".  In English, w34 means "week 34", not all
     languages have this concept. */
  m = matcher_new (_("^(?<task>.+) (?:by|due|on)? w(?<week>\\d{1,2})$"), match_week);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills in 4 days". */
  m = matcher_new (_("^(?<task>.+) in (?<days>\\d+) days?$"), match_days);
  if (m) matchers = g_list_prepend (matchers, m);

  /* Translators: "Pay bills in 2 weeks". */
  m = matcher_new (_("^(?<task>.+) in (?<weeks>\\d+) weeks?$"), match_weeks);
  if (m) matchers = g_list_prepend (matchers, m);

  return g_list_reverse (matchers);
}

GDate *
koto_date_parse (const char *s, char **out_s)
{
  static GList *matchers = NULL;
  GList  *l;

#if DEBUG
  g_printerr ("Testing '%s'\n", s);
#endif

  if (matchers == NULL)
    matchers = init_matchers ();

  for (l = matchers; l != NULL; l = l->next) {
    Matcher *m = l->data;
    GMatchInfo *info;
    GDate *date;

    if (g_regex_match (m->regex, s, 0, &info)) {
#if DEBUG
      g_printerr ("Matched with '%s'\n", g_regex_get_pattern (m->regex));
#endif
      date = m->func (s, info);
      if (!date)
	goto next;

      if (!g_date_valid (date)) {
	g_date_free (date);
	goto next;
      }

      if (out_s) {
	*out_s = g_match_info_fetch_named (info, "task");
#if DEBUG
	g_printerr ("Got task summary '%s'\n", *out_s);
#endif
      }
      g_match_info_free (info);
      return date;
    }
  next:
    g_match_info_free (info);
  }
  return NULL;
}

#if BUILD_TESTS

/* TODO: switch all this to use GTest? */

#include <locale.h>
#include <string.h>

static void
compare (GDate *expected, GDate *date)
{
  g_assert (g_date_valid (date));
  if (g_date_compare (date, expected) != 0) {
    char s1[32], s2[32];
    g_date_strftime (s1, sizeof (s1), "%x", expected);
    g_date_strftime (s2, sizeof (s2), "%x", date);
    g_printerr ("Incorrect date: expected %s got %s\n", s1, s2);
    g_assert_not_reached ();
  }
}

#define TEST_PARSER(text, expected_s) \
  s = NULL;                                     \
  date = koto_date_parse (text, &s);		\
  compare (expected, date);					\
  g_date_free (date);					\
  g_assert (strcmp (s, expected_s) == 0);               \
  g_free (s);

#define TEST_PARSER_FAIL(text) \
  s = NULL;                                     \
  date = koto_date_parse (text, &s);            \
  g_assert (date == NULL);                      \
  g_assert (s == NULL);


int
main (int argc, char **argv)
{
  GDate *expected, *date;
  char *s = NULL;

  /*
   * today() returns Tuesday 11th March 2008 when running the test case.
   */

  /* Set the locale to C so we can do string comparisons easily */
  setlocale (LC_ALL, "C");

  /* Sanity checks for non-date strings */

  TEST_PARSER_FAIL ("foo");
  TEST_PARSER_FAIL ("foo and bar");

  /* Today, tomorrow, yesterday */

  expected = today ();
  TEST_PARSER ("foo today", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("FOO TOMORROW", "FOO");

  g_date_subtract_days (expected, 2);
  TEST_PARSER ("Foo Yesterday", "Foo");

  /* On 21st, etc */

  g_date_set_dmy (expected, 1, 3, 2008);
  TEST_PARSER ("foo on 1st", "foo");

  g_date_set_dmy (expected, 21, 3, 2008);
  TEST_PARSER ("foo on 21st", "foo");

  g_date_set_dmy (expected, 22, 3, 2008);
  TEST_PARSER ("FOO BY 22nd", "FOO");

  g_date_set_dmy (expected, 23, 3, 2008);
  TEST_PARSER ("foo Due 23rd", "foo");

  TEST_PARSER_FAIL ("foo by 999th");
  TEST_PARSER_FAIL ("foo on 32th");

  /* On 1st June, etc */

  g_date_set_dmy (expected, 1, 4, 2008);
  TEST_PARSER ("foo on 1st april", "foo");
  TEST_PARSER ("foo on 1st apr", "foo");
  TEST_PARSER ("foo on apr 1st", "foo");

  g_date_set_dmy (expected, 2, 6, 2008);
  TEST_PARSER ("foo by 2nd June", "foo");
  TEST_PARSER ("FOO BY 2ND JUN", "FOO");
  TEST_PARSER ("Foo By June 2nd", "Foo");

  g_date_set_dmy (expected, 3, 1, 2008);
  TEST_PARSER ("FOO BY 3RD JANUARY", "FOO");
  TEST_PARSER ("foo by 3rd jan", "foo");
  TEST_PARSER ("foo by jan 3rd", "foo");

  /* This/etc [weekday] */

  g_date_set_dmy (expected, 12, 3, 2008);
  TEST_PARSER ("foo this wednesday", "foo");
  TEST_PARSER ("foo this wed", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo on Thursday", "foo");
  TEST_PARSER ("foo this thu", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo this FRIDAY", "foo");
  TEST_PARSER ("foo due FRI", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo this saturday", "foo");
  TEST_PARSER ("foo on sat", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo this Sunday", "foo");
  TEST_PARSER ("foo by Sun", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo this Monday", "foo");
  TEST_PARSER ("foo on Mon", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo this TUESDAY", "foo");
  TEST_PARSER ("foo by TUE", "foo");

  TEST_PARSER_FAIL ("foo on flob");
  TEST_PARSER_FAIL ("foo due fish");
  TEST_PARSER_FAIL ("foo by fish");
  TEST_PARSER_FAIL ("fish this flob");

  /* Next [weekday] */

  g_date_set_dmy (expected, 17, 3, 2008);
  TEST_PARSER ("foo next monday", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("Foo Next Tuesday", "Foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("FOO NEXT WEDNESDAY", "FOO");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo next thursday", "foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("Foo Next Friday", "Foo");

  g_date_add_days (expected, 1);
  TEST_PARSER ("FOO NEXT SATURDAY", "FOO");

  g_date_add_days (expected, 1);
  TEST_PARSER ("foo next sunday", "foo");

  TEST_PARSER_FAIL ("foo next flobday");

  /* Digits.  Remember we're in in the C locale so the dates are m/d/y */

  g_date_set_dmy (expected, 17, 3, 2008);
  TEST_PARSER ("foo on 3/17/08", "foo");
  g_date_set_dmy (expected, 29, 2, 2008);
  TEST_PARSER ("foo on 2/29/8", "foo");
  g_date_set_dmy (expected, 13, 4, 2008);
  TEST_PARSER ("foo on 4-13-08", "foo");
  g_date_set_dmy (expected, 1, 1, 2008);
  TEST_PARSER ("foo on 1-1-8", "foo");
  /* All out of range */
  TEST_PARSER_FAIL ("foo on 99/99/99");
  /* Invalid month */
  TEST_PARSER_FAIL ("foo on 13/1/08");
  /* Invalid day */
  TEST_PARSER_FAIL ("foo on 1/32/07");
  TEST_PARSER_FAIL ("foo on 2/29/07");
  TEST_PARSER_FAIL ("foo on 4/31/07");

  /* Week numbers */

  g_date_set_dmy (expected, 17, 3, 2008);
  TEST_PARSER ("foo on w12", "foo");

  g_date_set_dmy (expected, 26, 5, 2008);
  TEST_PARSER ("foo on w22", "foo");

  g_date_set_dmy (expected, 22, 12, 2008);
  TEST_PARSER ("foo on w52", "foo");

  g_date_set_dmy (expected, 29, 12, 2008);
  TEST_PARSER ("foo on w1", "foo");

  g_date_set_dmy (expected, 5, 1, 2009);
  TEST_PARSER ("foo on w2", "foo");

  g_date_set_dmy (expected, 9, 3, 2009);
  TEST_PARSER ("foo on w11", "foo");

  /* In x days */

  g_date_set_dmy (expected, 12, 3, 2008);
  TEST_PARSER ("foo in 1 day", "foo");

  g_date_set_dmy (expected, 13, 3, 2008);
  TEST_PARSER ("foo in 2 days", "foo");

  g_date_set_dmy (expected, 10, 4, 2008);
  TEST_PARSER ("foo in 30 days", "foo");

  g_date_set_dmy (expected, 11, 3, 2009);
  TEST_PARSER ("foo in 365 days", "foo");

  TEST_PARSER_FAIL ("foo in -10 days");
  TEST_PARSER_FAIL ("foo in 0 days");

  /* In x weeks */

  g_date_set_dmy (expected, 18, 3, 2008);
  TEST_PARSER ("foo in 1 week", "foo");

  g_date_set_dmy (expected, 25, 3, 2008);
  TEST_PARSER ("foo in 2 weeks", "foo");

  g_date_set_dmy (expected, 20, 5, 2008);
  TEST_PARSER ("foo in 10 weeks", "foo");

  TEST_PARSER_FAIL ("foo in -10 weeks");
  TEST_PARSER_FAIL ("foo in 0 weeks");

  g_date_free (expected);

  return 0;
}
#endif /* BUILD_TESTS */

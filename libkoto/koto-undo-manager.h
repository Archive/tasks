/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Iain Holmes <iain@gnome.org>
 *
 *  Copyright 2002 - 2006 Iain Holmes
 *
 *  This file is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU Library General Public
 *  License as published by the Free Software Foundation;
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __KOTO_UNDO_MANAGER_H__
#define __KOTO_UNDO_MANAGER_H__

#include <glib-object.h>
#include <glib.h>

#include "koto-undoable.h"

#define KOTO_TYPE_UNDO_MANAGER (koto_undo_manager_get_type ())
#define KOTO_UNDO_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOTO_TYPE_UNDO_MANAGER, KotoUndoManager))
#define KOTO_IS_UNDO_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOTO_TYPE_UNDO_MANAGER))

typedef struct _KotoUndoContext KotoUndoContext;
typedef struct _KotoUndoHistory {
	char *name;

	gboolean current; /* Is this the position we are at in the history */
	KotoUndoContext *ctxt;
} KotoUndoHistory;

typedef struct _KotoUndoManager KotoUndoManager;
typedef struct _KotoUndoManagerClass KotoUndoManagerClass;
typedef struct _KotoUndoManagerPrivate KotoUndoManagerPrivate;

struct _KotoUndoManager {
	GObject object;

	KotoUndoManagerPrivate *priv;
};

struct _KotoUndoManagerClass {
	GObjectClass parent_class;

	void (*changed) (KotoUndoManager *manager);
};

GType koto_undo_manager_get_type (void);
KotoUndoManager *koto_undo_manager_new (void);

gboolean koto_undo_manager_can_undo (KotoUndoManager *manager);
gboolean koto_undo_manager_can_redo (KotoUndoManager *manager);

KotoUndoContext *koto_undo_manager_context_begin (KotoUndoManager *manager,
						      const char *name);
KotoUndoContext *
koto_undo_manager_context_begin_formatted (KotoUndoManager *manager,
					   const char *format, ...);

void koto_undo_manager_context_end (KotoUndoManager *manager,
				      KotoUndoContext *ctxt);
void koto_undo_manager_context_cancel (KotoUndoManager *manager,
					 KotoUndoContext *ctxt);

void koto_undo_context_add (KotoUndoContext *context,
			      KotoUndoable *undoable);
const char *koto_undo_manager_get_undo_name (KotoUndoManager *manager);
const char *koto_undo_manager_get_redo_name (KotoUndoManager *manager);

void koto_undo_manager_undo (KotoUndoManager *manager);
void koto_undo_manager_redo (KotoUndoManager *manager);

GList *koto_undo_manager_get_history (KotoUndoManager *manager);

#endif

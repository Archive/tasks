#include <string.h>
#include "koto-group-store.h"

int main (int argc, char **argv)
{
  KotoGroupStore *store;
  GtkTreeIter iter;

  g_type_init ();

  store = (KotoGroupStore*)koto_group_store_new (NULL);
  g_assert (store);

  koto_group_store_add_new_category (store, NULL, "Foo");
  koto_group_store_add_new_category (store, NULL, "Bar");

  g_assert (koto_group_store_get_iter_for_group (store, "Foo", &iter));
  g_assert (koto_group_store_get_iter_for_group (store, "Bar", &iter));
  g_assert (koto_group_store_get_iter_for_group (store, "Flob", &iter) == FALSE);

  g_assert (koto_group_store_match_group (store, "Flob") == NULL);
  g_assert (strcmp (koto_group_store_match_group (store, "Foo"), "Foo") == 0);
  g_assert (strcmp (koto_group_store_match_group (store, "foo"), "Foo") == 0);
  g_assert (strcmp (koto_group_store_match_group (store, "baR"), "Bar") == 0);

  return 0;
}

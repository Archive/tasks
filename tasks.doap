<Project xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns="http://usefulinc.com/ns/doap#">

  <name>Tasks</name>
  <shortname>tasks</shortname>
  <created>2007-02-20</created>

  <shortdesc xml:lang="en">
    Tasks is a lightweight To Do list manager.  It uses libecal, part of EDS.
  </shortdesc>

  <description xml:lang="en">
    Tasks is a simple To Do manager, using GTK+ and libecal (part of EDS).  It
    eschews complicated features for a simple interface and functionality that
    does the right thing.
  </description>

  <license rdf:resource="http://usefulinc.com/doap/licenses/gpl" />

  <homepage rdf:resource="http://pimlico-project.org" />
  <download-page rdf:resource="http://pimlico-project.org/sources/tasks" />

  <os>Linux</os>
  <programming-language>C</programming-language>

  <category rdf:resource="http://labs.o-hand.com/doap/category/application"/>

  <maintainer>
    <foaf:Person>
      <foaf:name>Ross Burton</foaf:name>
      <foaf:mbox rdf:resource="mailto:ross@openedhand.com"/>
    </foaf:Person>
  </maintainer>

  <bug-database rdf:resource="http://bugzilla.openedhand.com/"/>

  <repository>
    <SVNRepository>
      <browse rdf:resource="http://svn.o-hand.com/view/tasks/trunk"/>
      <location rdf:resource="https://svn.o-hand.com/repos/tasks/trunk/"/>
    </SVNRepository>
  </repository>

  <release>
    <Version>
      <created>2007-02-23</created>
      <revision>0.1</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.1.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-02-28</created>
      <revision>0.2</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.2.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-03-05</created>
      <revision>0.3</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.3.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-03-15</created>
      <revision>0.4</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.4.tar.gz" />
    </Version>
  </release>
  
  <release>
    <Version>
      <created>2007-05-19</created>
      <revision>0.5</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.5.tar.gz" />
    </Version>
  </release>
  
  <release>
    <Version>
      <created>2007-05-23</created>
      <revision>0.6</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.6.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-05-27</created>
      <revision>0.7</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.7.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-06-14</created>
      <revision>0.8</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.8.tar.gz" />
    </Version>
  </release>
  
  <release>
    <Version>
      <created>2007-06-26</created>
      <revision>0.9</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.9.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-07-11</created>
      <revision>0.10</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.10.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-08-13</created>
      <revision>0.11</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.11.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-09-23</created>
      <revision>0.12</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.12.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2007-12-06</created>
      <revision>0.13</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.13.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2008-08-29</created>
      <revision>0.14</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.14.tar.gz" />
    </Version>
  </release>

  <release>
    <Version>
      <created>2009-03-30</created>
      <revision>0.15</revision>
      <file-release rdf:resource="http://pimlico-project.org/sources/tasks/tasks-0.15.tar.gz" />
    </Version>
  </release>
  
</Project>
